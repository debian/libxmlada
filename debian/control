Source: libxmlada
Priority: optional
Section: libdevel
Maintainer: Ludovic Brenta <lbrenta@debian.org>
Uploaders: Xavier Grave <xavier.grave@csnsm.in2p3.fr>,
 Nicolas Boulenguez <nicolas@debian.org>
Build-Depends:
 debhelper-compat (= 13),
Build-Depends-Arch:
 dh-ada-library (>= 9.1),
 dh-sequence-ada-library,
 gnat,
 gnat-14,
 unicode-data,
# Used by debian/rules to generate unicode-names-*.ads.
# Hence, new unicode versions change the sources and the ALI version.
Build-Depends-Indep:
 dh-sequence-sphinxdoc,
 python3-sphinx (>= 1.6.3-2),
# fixing #869098.
 python3-sphinx-rtd-theme,
 latexmk,
# sphinx-doc requires tgheros.sty:
 tex-gyre,
# Sphinx >= 1.6 uses the latexmk driver.
 texlive-fonts-recommended,
# sphinx-doc requires iftex.sty:
 texlive-plain-generic,
 texlive-latex-extra,
Standards-Version: 4.7.0
Rules-Requires-Root: no
Vcs-Browser: https://salsa.debian.org/debian/libxmlada
Vcs-Git: https://salsa.debian.org/debian/libxmlada.git
Homepage: https://github.com/AdaCore/xmlada

Package: libxmlada-unicode-dev
Breaks: libxmlada-unicode8-dev, libxmlada-unicode9-dev, libxmlada-unicode10-dev,
 libxmlada-unicode11-dev, libxmlada-unicode12-dev
Replaces: libxmlada-unicode8-dev, libxmlada-unicode9-dev, libxmlada-unicode10-dev,
 libxmlada-unicode11-dev, libxmlada-unicode12-dev
Provides: ${ada:Provides}
Architecture: any
Depends: ${misc:Depends}, ${ada:Depends}
Suggests: libxmlada-doc
# so all -dev packages indirectly Suggest: -doc.
Description: XML/Ada, a full XML suite for Ada programmers (unicode)
 XML/Ada is a set of modules that provide a simple manipulation of XML
 streams. It supports the whole XML 1.0 specifications, and can parse
 any file that follows this standard (including the contents of the
 DTD).  It also provides support for a number of other standard
 associated with XML, like SAX, DOM, and XML Schemas.  In addition, it
 includes a module to manipulate Unicode streams, since this is required
 by the XML standard.
 .
 This package contains the development tools for the unicode module.

Package: libxmlada-doc
Section: doc
Architecture: all
Multi-Arch: foreign
Depends: ${misc:Depends}, ${sphinxdoc:Depends}
Built-Using: ${sphinxdoc:Built-Using}
Suggests: gnat
Description: XML/Ada, a full XML suite for Ada programmers (documentation)
 XML/Ada is a set of modules that provide a simple manipulation of XML
 streams. It supports the whole XML 1.0 specifications, and can parse
 any file that follows this standard (including the contents of the
 DTD).  It also provides support for a number of other standard
 associated with XML, like SAX, DOM, and XML Schemas.  In addition, it
 includes a module to manipulate Unicode streams, since this is required
 by the XML standard.
 .
 This package contains the documentation in text, PDF and HTML.

Package: libxmlada-dom-dev
Breaks: libxmlada-dom8-dev, libxmlada-dom9-dev, libxmlada-dom10-dev,
 libxmlada-dom11-dev, libxmlada-dom12-dev
Replaces: libxmlada-dom8-dev, libxmlada-dom9-dev, libxmlada-dom10-dev,
 libxmlada-dom11-dev, libxmlada-dom12-dev
Provides: ${ada:Provides}
Architecture: any
Depends: ${misc:Depends}, ${ada:Depends}
Description: XML/Ada, a full XML suite for Ada programmers (dom)
 XML/Ada is a set of modules that provide a simple manipulation of XML
 streams. It supports the whole XML 1.0 specifications, and can parse
 any file that follows this standard (including the contents of the
 DTD).  It also provides support for a number of other standard
 associated with XML, like SAX, DOM, and XML Schemas.  In addition, it
 includes a module to manipulate Unicode streams, since this is required
 by the XML standard.
 .
 This package contains the development tools for the dom module.

Package: libxmlada-dom9
Section: libs
Architecture: any
Depends: ${shlibs:Depends}, ${misc:Depends}
Multi-Arch: same
Description: XML/Ada, a full XML suite for Ada programmers (dom runtime)
 XML/Ada is a set of modules that provide a simple manipulation of XML
 streams. It supports the whole XML 1.0 specifications, and can parse
 any file that follows this standard (including the contents of the
 DTD).  It also provides support for a number of other standard
 associated with XML, like SAX, DOM, and XML Schemas.  In addition, it
 includes a module to manipulate Unicode streams, since this is required
 by the XML standard.
 .
 This package contains the dom runtime shared library.

Package: libxmlada-input-dev
Breaks: libxmlada-input8-dev, libxmlada-input9-dev, libxmlada-input10-dev,
 libxmlada-input11-dev, libxmlada-input12-dev
Replaces: libxmlada-input8-dev, libxmlada-input9-dev, libxmlada-input10-dev,
 libxmlada-input11-dev, libxmlada-input12-dev
Provides: ${ada:Provides}
Architecture: any
Depends: ${misc:Depends}, ${ada:Depends}
Description: XML/Ada, a full XML suite for Ada programmers (input sources)
 XML/Ada is a set of modules that provide a simple manipulation of XML
 streams. It supports the whole XML 1.0 specifications, and can parse
 any file that follows this standard (including the contents of the
 DTD).  It also provides support for a number of other standard
 associated with XML, like SAX, DOM, and XML Schemas.  In addition, it
 includes a module to manipulate Unicode streams, since this is required
 by the XML standard.
 .
 This package contains the development tools for the input sources module.

Package: libxmlada-input8
Section: libs
Architecture: any
Depends: ${shlibs:Depends}, ${misc:Depends}
Multi-Arch: same
Description: XML/Ada, a full XML suite for Ada programmers (input sources runtime)
 XML/Ada is a set of modules that provide a simple manipulation of XML
 streams. It supports the whole XML 1.0 specifications, and can parse
 any file that follows this standard (including the contents of the
 DTD).  It also provides support for a number of other standard
 associated with XML, like SAX, DOM, and XML Schemas.  In addition, it
 includes a module to manipulate Unicode streams, since this is required
 by the XML standard.
 .
 This package contains the input sources runtime shared library.

Package: libxmlada-sax-dev
Breaks: libxmlada-sax8-dev, libxmlada-sax9-dev, libxmlada-sax10-dev,
 libxmlada-sax11-dev, libxmlada-sax12-dev
Replaces: libxmlada-sax8-dev, libxmlada-sax9-dev, libxmlada-sax10-dev,
 libxmlada-sax11-dev, libxmlada-sax12-dev
Provides: ${ada:Provides}
Architecture: any
Depends: ${misc:Depends}, ${ada:Depends}
Description: XML/Ada, a full XML suite for Ada programmers (sax)
 XML/Ada is a set of modules that provide a simple manipulation of XML
 streams. It supports the whole XML 1.0 specifications, and can parse
 any file that follows this standard (including the contents of the
 DTD).  It also provides support for a number of other standard
 associated with XML, like SAX, DOM, and XML Schemas.  In addition, it
 includes a module to manipulate Unicode streams, since this is required
 by the XML standard.
 .
 This package contains the development tools for the sax module.

Package: libxmlada-sax8
Section: libs
Architecture: any
Depends: ${shlibs:Depends}, ${misc:Depends}
Multi-Arch: same
Description: XML/Ada, a full XML suite for Ada programmers (sax runtime)
 XML/Ada is a set of modules that provide a simple manipulation of XML
 streams. It supports the whole XML 1.0 specifications, and can parse
 any file that follows this standard (including the contents of the
 DTD).  It also provides support for a number of other standard
 associated with XML, like SAX, DOM, and XML Schemas.  In addition, it
 includes a module to manipulate Unicode streams, since this is required
 by the XML standard.
 .
 This package contains the sax runtime shared library.

Package: libxmlada-schema-dev
Breaks: libxmlada-schema8-dev, libxmlada-schema9-dev, libxmlada-schema10-dev,
 libxmlada-schema11-dev, libxmlada-schema12-dev
Replaces: libxmlada-schema8-dev, libxmlada-schema9-dev, libxmlada-schema10-dev,
 libxmlada-schema11-dev, libxmlada-schema12-dev
Provides: ${ada:Provides}
Architecture: any
Depends: ${misc:Depends}, ${ada:Depends}
Description: XML/Ada, a full XML suite for Ada programmers (schema)
 XML/Ada is a set of modules that provide a simple manipulation of XML
 streams. It supports the whole XML 1.0 specifications, and can parse
 any file that follows this standard (including the contents of the
 DTD).  It also provides support for a number of other standard
 associated with XML, like SAX, DOM, and XML Schemas.  In addition, it
 includes a module to manipulate Unicode streams, since this is required
 by the XML standard.
 .
 This package contains the development tools for the schema module,
 and an xmlada.gpr project importing the whole XML/Ada library.
# inside schema-dev because it Depends: every other -dev package.

Package: libxmlada-schema8
Section: libs
Architecture: any
Depends: ${shlibs:Depends}, ${misc:Depends}
Multi-Arch: same
Description: XML/Ada, a full XML suite for Ada programmers (schema runtime)
 XML/Ada is a set of modules that provide a simple manipulation of XML
 streams. It supports the whole XML 1.0 specifications, and can parse
 any file that follows this standard (including the contents of the
 DTD).  It also provides support for a number of other standard
 associated with XML, like SAX, DOM, and XML Schemas.  In addition, it
 includes a module to manipulate Unicode streams, since this is required
 by the XML standard.
 .
 This package contains the schema runtime shared library.

Package: libxmlada-unicode8
Section: libs
Architecture: any
Depends: ${shlibs:Depends}, ${misc:Depends}
Multi-Arch: same
Description: XML/Ada, a full XML suite for Ada programmers (unicode runtime)
 XML/Ada is a set of modules that provide a simple manipulation of XML
 streams. It supports the whole XML 1.0 specifications, and can parse
 any file that follows this standard (including the contents of the
 DTD).  It also provides support for a number of other standard
 associated with XML, like SAX, DOM, and XML Schemas.  In addition, it
 includes a module to manipulate Unicode streams, since this is required
 by the XML standard.
 .
 This package contains the unicode runtime shared library.
