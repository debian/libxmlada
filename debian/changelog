libxmlada (25.0.0-3) experimental; urgency=medium

  * Force gnat-14 in Build-Depends (else unstable is preferred).

 -- Nicolas Boulenguez <nicolas@debian.org>  Thu, 19 Dec 2024 09:50:30 +0000

libxmlada (25.0.0-2) experimental; urgency=medium

  * Rebuild in experimental, gnat (>= 14) is now really available.
  * Drop the version restrictions in the unicode-data build-dependency.
  * Update exceptions for Unicode 16 block names.

 -- Nicolas Boulenguez <nicolas@debian.org>  Sat, 07 Dec 2024 09:52:18 +0000

libxmlada (25.0.0-1) experimental; urgency=medium

  * New upstream version.
  * Build with gnat-14 in experimental.
  * Reduce diff with gprbuild/debian/rules.

 -- Nicolas Boulenguez <nicolas@debian.org>  Sat, 19 Oct 2024 13:12:41 +0200

libxmlada (24.0.0-2) unstable; urgency=medium

  * Reupload to unstable for the gnat-13/time_t64 transition.

 -- Nicolas Boulenguez <nicolas@debian.org>  Sun, 17 Mar 2024 14:37:57 +0100

libxmlada (24.0.0-1) experimental; urgency=medium

  * New upstream release, depending on sphinx rtd theme.
  * Rebuild with dh-ada-library 9.1, with gnat versions in ada:Depends.

 -- Nicolas Boulenguez <nicolas@debian.org>  Sun, 17 Dec 2023 11:58:49 +0000

libxmlada (23.0.0-6) experimental; urgency=medium

  * Bump shared object versions. Gprbuild/2023.0.0-3 (gcc-12) crashes when
    linking with xmlada/23.0.0-5 (gcc-13) instead of 23.0.0-3 (gcc-12).

 -- Nicolas Boulenguez <nicolas@debian.org>  Fri, 27 Oct 2023 10:53:44 +0000

libxmlada (23.0.0-5) experimental; urgency=medium

  * Typo in Break/Replace for libxmlada-unicode-dev. Closes: #1054530.

 -- Nicolas Boulenguez <nicolas@debian.org>  Wed, 25 Oct 2023 11:19:35 +0000

libxmlada (23.0.0-4) experimental; urgency=medium

  * Build in experimental with gnat >= 13 and dh-ada-library >= 9.
  * Rename -dev package, now unversioned. Provide a name containing a hash.
  * Build-Depend: gnat instead of gnat-13. Break/Replace previous -devs.

 -- Nicolas Boulenguez <nicolas@debian.org>  Tue, 24 Oct 2023 16:28:48 +0000

libxmlada (23.0.0-3) unstable; urgency=medium

  * Break/Replace libxmlada*-dev since oldstable.
    Closes: #1034918, #1034926, #1034937, #1034944, #1034952.

 -- Nicolas Boulenguez <nicolas@debian.org>  Wed, 10 May 2023 21:52:37 +0200

libxmlada (23.0.0-2) unstable; urgency=medium

  * Reupload to unstable for the gnat-12 transition.
  * Standards-Version: 4.6.2 (no changes).

 -- Nicolas Boulenguez <nicolas@debian.org>  Mon, 26 Dec 2022 23:13:00 +0100

libxmlada (23.0.0-1) experimental; urgency=medium

  * New upstream release.
  * Build against unicode-data 15.  Closes: #1020018.
    Do not rename the -dev package in experimental.

 -- Nicolas Boulenguez <nicolas@debian.org>  Thu, 24 Nov 2022 08:52:59 +0000

libxmlada (23~20220806-3) experimental; urgency=medium

  * Avoid variables set by packaging.mk during -indep builds.

 -- Nicolas Boulenguez <nicolas@debian.org>  Mon, 29 Aug 2022 18:10:48 +0000

libxmlada (23~20220806-2) experimental; urgency=medium

  * Simplify debian/rules thanks to dh-ada-library 8.1.
  * Actually refresh the unicode generated sources.
  * patches/unicode-exceptions.diff for unicode 14.
  * In debian/rules, write a bootstap path and not a dependency graph.
    Also remove unneeded subdirectories.
  * Prefer make to dh_auto_build because the latter interfers with
    gnatmake or sphinx parallelism.
  * Fix ALI path in tests/no-project.

 -- Nicolas Boulenguez <nicolas@debian.org>  Thu, 18 Aug 2022 21:16:27 +0000

libxmlada (23~20220806-1) experimental; urgency=medium

  * New upstream snapshot merging all Debian patches.
  * Use DEB_GNAT_VERSION from debian_packaging.mk
  * Move clean instructions into debian/clean

 -- Nicolas Boulenguez <nicolas@debian.org>  Sun, 07 Aug 2022 23:37:45 +0000

libxmlada (23~20220614-1) experimental; urgency=medium

  * Build new upstream snapshot with gnat-12 in experimental.
    Change ALI versions accordingly.  Change SO version of the dom component.
  * Fix regular expression removing -prefix-map lines from .ali files.
  * Avoid failing when erroneous execution rights are already removed.
  * Standards-Version: 4.6.1.

 -- Nicolas Boulenguez <nicolas@debian.org>  Sat, 06 Aug 2022 14:39:08 +0200

libxmlada (22.0.0-3) unstable; urgency=medium

  * Reupload to unstable for the gnat-11 transition
  * Configure branch names for git-buildpackage

 -- Nicolas Boulenguez <nicolas@debian.org>  Sun, 08 May 2022 17:50:29 +0200

libxmlada (22.0.0-2) experimental; urgency=medium

  * Let -dev packages depend on 'gnat (>= X), gnat (<< X+1)' instead of
    'gnat, gnat-X'.  See https://bugs.debian.org/bug=975589#24.
  * Override dh_gencontrol instead of appending to .substvars, in order to
    increase log readability and prevent multiple inclusions.

 -- Nicolas Boulenguez <nicolas@debian.org>  Tue, 26 Apr 2022 12:31:28 +0000

libxmlada (22.0.0-1) experimental; urgency=medium

  * Rebuild with gnat-11 in experimental.
    Build-Depend: unicode 14 (closes: #995578).
    New upstream version.
    Rename -dev packages per Ada policy. No need to bump soversion.
  * Build-Depend: tex-gyre for sphinx documentation.
  * Split unicode updates in small patches instead of rebasing.
  * Standards-Version: 4.6.0.
  * Add license for Ada code generated from Unicode data.
  * Add upstream metadata.
  * watch: remove unneeded filenamemangle uscan option.

 -- Nicolas Boulenguez <nicolas@debian.org>  Sun, 13 Mar 2022 11:17:22 +0000

libxmlada (21.0.0-4) unstable; urgency=medium

  * In autopkgtests, import the installed xmlada.gpr instead of the one in
    the top source directory where tests are run. Closes: #977550.

 -- Nicolas Boulenguez <nicolas@debian.org>  Sun, 20 Dec 2020 18:07:52 +0000

libxmlada (21.0.0-3) unstable; urgency=medium

  * Fix autopkgtests: syntax error in gnat project.

 -- Nicolas Boulenguez <nicolas@debian.org>  Sun, 20 Dec 2020 11:22:18 +0000

libxmlada (21.0.0-2) unstable; urgency=medium

  * Reupload to unstable for gnat-10 transition.

 -- Ludovic Brenta <lbrenta@debian.org>  Fri, 11 Dec 2020 09:08:45 +0100

libxmlada (21.0.0-1) experimental; urgency=medium

  * New upstream release (no renaming needed).
  * AdaCore grants the GCC runtime exception to the GPL-3+ license.
  * AdaCore provides GitHub releases, replace README.source with watch file.
  * The archives contain no minified javascript anymore.  Stop repackaging.
  * Merge upstream and debian copyright stanzas.
    Add missing license for documentation.
  * Let run time tests check availability of examples in -doc package.
  * For simplicity, override dh_auto_configure instead of dh_fixperms.
  * Disable parallelism for docs (unreliable cooperation of make and sphinx).

 -- Nicolas Boulenguez <nicolas@debian.org>  Mon, 19 Oct 2020 17:34:46 +0000

libxmlada (20-1) experimental; urgency=medium

  * New upstream release built with gnat-10.
    Migration in experimental, renaming lib and -dev packages.
  * Debhelper 13.
  * Drop obsolete misc:Pre-Depends.
  * Simplify dependency handling in debian/rules.
  * Force Make order so that gnatmake does not compile the same source twice.
  * Stop overwriting dynamic .ali with static.ali.
  * Strip unreproducible -fdebug-prefix-map options from installed .ali files.

 -- Nicolas Boulenguez <nicolas@debian.org>  Wed, 20 May 2020 19:55:13 +0000

libxmlada (19-3) unstable; urgency=medium

  * Update for unicode 13. Closes: #954130.
    The changes affect niche packages, so I am keeping the ALI version.
  * Move gnat and unicode-data from Build-Depends to Build-Depends-Arch.

 -- Nicolas Boulenguez <nicolas@debian.org>  Wed, 25 Mar 2020 16:16:24 +0100

libxmlada (19-2) unstable; urgency=medium

  * Reupload to unstable for gnat-9 transition.
  * Drop --as-needed, enabled by default with gcc-9.
  * Only include used dpkg Makefile snippets.
  * Debhelper 12 does not compress examples anymore.
  * Reduce divergence with part of d/rules shared with gprbuild.
  * Remove redundant branch from Vcs-Git field.
  * Standards-Version: 4.5.0.
  * Use dh-sequence debhelper syntax for sphinxdoc.

 -- Nicolas Boulenguez <nicolas@debian.org>  Sun, 23 Feb 2020 17:51:37 +0100

libxmlada (19-1) experimental; urgency=medium

  * New upstream release, built with gnat-9.
    Migration in experimental, renaming lib and -dev packages.
  * Move Version Control System to salsa.
  * Refresh forwarded patches.
  * Debhelper 12.
  * Standards-version 4.4.0.
  * Update Source URL in d/copyright.

 -- Nicolas Boulenguez <nicolas@debian.org>  Thu, 18 Jul 2019 11:18:48 +0200

libxmlada (18-4) unstable; urgency=medium

  * Update to unicode version 12. Closes: #927943.

 -- Nicolas Boulenguez <nicolas@debian.org>  Thu, 02 May 2019 11:47:15 +0200

libxmlada (18-3) unstable; urgency=medium

  * Update to unicode version 11, really closing #903380.
    The SO and ALI versions are already new in unstable.
  * Standards-Version: 4.2.1. Rules-Requires-Root: no
  * Add some warning to build flags.

 -- Nicolas Boulenguez <nicolas@debian.org>  Sat, 27 Oct 2018 19:30:35 +0200

libxmlada (18-2) unstable; urgency=medium

  * Rebuild in unstable for gnat-8 transition. Closes: #903380.

 -- Nicolas Boulenguez <nicolas@debian.org>  Sat, 27 Oct 2018 15:10:06 +0200

libxmlada (18-1) experimental; urgency=medium

  * New upstream release. No package renaming in experimental.
  * Enable parallel doc builds now that #869098 is closed.
  * Remove execution permissions for example sources.

 -- Nicolas Boulenguez <nicolas@debian.org>  Sat, 09 Jun 2018 19:44:04 +0200

libxmlada (17.1.2017-7) experimental; urgency=medium

  * Rebuild with gnat-8.
    Rename shared library and -dev packages per Debian Ada policy.
  * Delete watch file. Explain why in README.source.

 -- Nicolas Boulenguez <nicolas@debian.org>  Mon, 07 May 2018 16:50:15 +0200

libxmlada (17.1.2017-6) unstable; urgency=medium

  * Drop .mtn-ignore, source/lintian-overrides.
    Keeping duplicate information in sync is wasted work.
  * Delete trailing spaces in this changelog.
  * Debhelper 11.
  * Drop obsolete version restriction for dpkg-dev.
  * Standards-Version: 4.1.4.
  * Update upstream homepage URL.
  * Add forgotten sphinxdoc:Build-Using substitution variable.
  * Increase verbosity of run time test.

 -- Nicolas Boulenguez <nicolas@debian.org>  Thu, 03 May 2018 17:16:15 +0200

libxmlada (17.1.2017-5) unstable; urgency=medium

  * Build-Depend latexmk for LaTeX sphinx-doc module. Closes: #872231.
  * Regenerate from unicode version 10. Build-Depend an unicode version
    instead of updating Ada sources with the same -dev package aliversion.
  * Build-Depend: python3 version of sphinx-doc.
  * Forward patches.
  * Standards-Version: 4.1.0.
  * Vcs-Mtn: new monotone URI format.

 -- Nicolas Boulenguez <nicolas@debian.org>  Mon, 25 Sep 2017 10:29:55 +0200

libxmlada (17.1.2017-4) unstable; urgency=medium

  * Upload to unstable.
  * Rename libxmlada-input4.5.2015 -> 4.6, forgotten in last upload.
  * Build-Depends: handle texlive generic package renaming.
  * Standards-Version: 4.0.1 (no changes).

 -- Nicolas Boulenguez <nicolas@debian.org>  Wed, 09 Aug 2017 18:08:06 +0200

libxmlada (17.1.2017-3) experimental; urgency=medium

  * Rename libxmlada-dom4.5.2015 -> 4.6, reflecting the API and compiler
    changes since last upload to unstable.
  * Disable sphinxdoc parallelism to prevent a FTBFS caused by #869098.
  * Drop Break/Replace: libxmlada-*6-dev, now implicit through gnat-7.
  * Drop Break/Replace: libxmlada-dev, not in oldstable anymore.
  * Debhelper 10. Drop explicit rules targets confusing dh.
  * Handle DEB_BUILD_OPTIONS=nodoc.

 -- Nicolas Boulenguez <nicolas@debian.org>  Tue, 25 Jul 2017 21:52:03 +0200

libxmlada (17.1.2017-2) experimental; urgency=medium

  * Build-Depend: unicode-data >= 9 instead of = 9.
  * Merge changes from gprbuild in shared portion of d/rules.

 -- Nicolas Boulenguez <nicolas@debian.org>  Sun, 02 Jul 2017 20:19:42 +0200

libxmlada (17.1.2017-1) experimental; urgency=medium

  * New upstream release. Build with gnat-7. No ALI/SO bump in experimental.
  * Standards-Version: 4.0.0. Suggest: -doc instead of Recommend.
  * Allow -doc package to satisfy Multi-Arch: foreign dependencies.
  * Move projects to /usr/share/gpr, in gprbuild's upstream lookup path.
  * Output of pdflatex and gnat are now reproducible without help.

 -- Nicolas Boulenguez <nicolas@debian.org>  Thu, 22 Jun 2017 09:33:11 +0200

libxmlada (4.6.2016-1) experimental; urgency=medium

  * New upstream release.
    ALI version of unicode increases, forcing all ALI versions to do so.
    Rename -dev and Break/Replace: previous versions per Ada policy.
    Only sax and schema SO binary interfaces may have changed.
    Since "input_sources" needs to be renamed, use upstream name "input".
  * Upstream drops xmlada-config, remove the related packaging stuff.
  * Apply generate-unicode-names.diff from upstream VCS.
    Exclude generated sources from repackaged orig tarball.
    Update it for unicode version 9 as packaged in Debian.
  * Allow a SO version per library and non existing lib/obj directories.
  * Ensure deterministic timestamps in patched or generated ALI files.
  * README.source: upstream VCS is now on GitHub.
  * Switch watch file to version 4.
  * Reorder build flags to simplify maintenance of PIE flags.
  * Generate internal dependencies instead of copying them manually.
  * tests/link-with-shared depends gprbuild for project support.

 -- Nicolas Boulenguez <nicolas@debian.org>  Thu, 11 Aug 2016 00:56:55 +0200

libxmlada (4.5.2015-8) unstable; urgency=medium

  * Build-Depend iftex.sty from texlive-generic-extra. Closes: #830395.

 -- Nicolas Boulenguez <nicolas@debian.org>  Mon, 11 Jul 2016 13:54:06 +0200

libxmlada (4.5.2015-7) unstable; urgency=medium

  [Martin Pitt]
  * Declare that tests/no-project depends on dpkg-dev, GCC. Closes: 823998.

 -- Nicolas Boulenguez <nicolas@debian.org>  Sat, 14 May 2016 16:03:39 +0200

libxmlada (4.5.2015-6) unstable; urgency=medium

  * Upload to unstable.
  * Standards-Versions: 3.9.8 (no changes).

 -- Nicolas Boulenguez <nicolas@debian.org>  Mon, 25 Apr 2016 23:36:04 +0200

libxmlada (4.5.2015-5) experimental; urgency=medium

  * Add more libraries versions (checked with apt-cudf in pbuilder).

 -- Nicolas Boulenguez <nicolas@debian.org>  Mon, 18 Apr 2016 21:24:01 +0200

libxmlada (4.5.2015-4) experimental; urgency=medium

  * Select experimental versions >= 6~ of GCC libraries, so that the
    dependency resolver on build daemons manages to install gnat-6.
  * Select gnatgcc in case it differs from gcc.

 -- Nicolas Boulenguez <nicolas@debian.org>  Sun, 17 Apr 2016 10:27:52 +0200

libxmlada (4.5.2015-3) experimental; urgency=medium

  * Rebuild with gnat-6.
  * Implement our own dh-ada-library to break circular build-dependency.
  * Enable all hardening flags, except PIE for the relocatable library.
  * Replace explicit -dbg packages with automatic -dbgsym packages.
  * Update to Standards-Versions 3.9.7 (no changes).

 -- Nicolas Boulenguez <nicolas@debian.org>  Wed, 09 Mar 2016 00:19:13 +0100

libxmlada (4.5.2015-2) experimental; urgency=medium

  * Arch-only builds should not require sphinx-common.

 -- Nicolas Boulenguez <nicolas@debian.org>  Fri, 30 Oct 2015 19:23:06 +0100

libxmlada (4.5.2015-1) experimental; urgency=medium

  * New upstream release GPL 2015 (between 4.5 and 4.6), built with gnat-5.
    Split libraries, change library and -dev packages versions per Ada policy.
    Avoid projects during build: gnatmake is dropping them, and
    gprbuild would cause a circular build dependency.
    Use Files-Excluded to spare half the size, avoid minified javascript,
    drop Build-Dependency on autotools-dev and autoconf.
  * Freeze the clock for deterministic PDF timestamps.
  * Use dh-sphinxdoc.
  * Patch SO version from debian/rules instead of a static patch.
  * control/Testsuite: obsolete.
  * watch: use Debian redirector to detect next GPL release.
    README.source, find_branchpoint.sh: describe what to do then.
  * Test project, command line and xmlada-config.

 -- Nicolas Boulenguez <nicolas@debian.org>  Sat, 26 Sep 2015 14:51:55 +0200

libxmlada (4.4.2014-1) unstable; urgency=medium

  * New upstream release GPL 2014 (between 4.4 and 4.5).
    SO version does not change, but ALI version does. Per Ada policy,
    dev package is renamed and Breaks/Replaces with previous ones

 -- Nicolas Boulenguez <nicolas@debian.org>  Sun, 03 Aug 2014 20:27:26 +0200

libxmlada (4.4.0puregpl-1) unstable; urgency=medium

  * The actual license does not grant the GCC Runtime Library Exception.
    Switched back to a branch for the orig archive, see debian/README.source.
    Obsoletes: license patch, override removing cvs-ignore.
  * patches/compile_with_4.6.diff: not needed anymore with gnat-4.9.

 -- Nicolas Boulenguez <nicolas@debian.org>  Fri, 02 May 2014 17:52:44 +0200

libxmlada (4.4.0-1) unstable; urgency=medium

  * New upstream release.
  * License clarified: GPL-3+ with GCC Runtime Library Exception.
  * Docs now formatted by sphinx. Symlink embedded javascript when possible.
  * xmlada-config relies on upstream configure again.
  * rules: run-tests target obsoleted by sadt from devscripts.

 -- Nicolas Boulenguez <nicolas@debian.org>  Thu, 01 May 2014 13:24:20 +0200

libxmlada (4.1-4) unstable; urgency=low

  * Conflics -> Breaks for previous versions per Ada policy.
    Specify versions for Breaks and Replaces (old package split).
  * rules: switch to dh_ada_library.
  * tests: updated to latest autopkg specification (cosmetic).
  * Removed work-around for GCC bug on ia64 (unreproducible with 4.6).

 -- Nicolas Boulenguez <nicolas@debian.org>  Thu, 29 Aug 2013 04:21:02 +0200

libxmlada (4.1-3) unstable; urgency=low

  * Switch to dpkg-source 3.0 (quilt) format.
  * Switch to debhelper 9 dh driver.
  * Standards-Version 3.9.4. No more DM-Upload-Allowed, copyright file format.
  * Rely on snippets provided by recent dpkg-dev and dh-ada-library to
    deal with architecture, hardening, noopt and parallel options.
  * Install examples. Use one as run-time test of dynamic linking.
  * Added libxmlada-doc.doc-base.xmlada, watch (informative only).
  * debian/patches/texinfo5.diff:  Closes: #713299.
  * xmlada-config,.gpr: generated, replace usr/lib with multiarch path.
  * Do not compress examples, install TODO in the -doc package.
  * Added myself to uploaders.

 -- Nicolas Boulenguez <nicolas@debian.org>  Fri, 16 Aug 2013 02:12:43 +0200

libxmlada (4.1-2) unstable; urgency=low

  * debian/patches/xmlada-config.patch: bump version number.
    Closes: #661593.
  * debian/control: remove references to ada-compiler.

 -- Ludovic Brenta <lbrenta@debian.org>  Tue, 28 Feb 2012 19:59:42 +0100

libxmlada (4.1-1) unstable; urgency=low

  [Xavier Grave]
  * New upstream version 4.1
  * add get-orig-source in debian/rules
  * Package renamings pursuant to Debian Policy for Ada:
    libxmlada3.2     -> libxmlada4.1
    libxmlada3.2-dev -> libxmlada4.1-dev
    libxmlada3.2-dbg -> libxmlada4.1-dbg

  [Ludovic Brenta]
  * debian/control (Architecture): any.
    (Uploaders): add Xavier.
    (DM-Upload-Allowed): yes.
    (Standards-Version): move to 3.9.2 with no changes required.
    (Multi-Arch): same.
    (Build-Depends): debhelper (>= 8.1.3).
  * debian/rules: install the shared and static libs in the multiarch
    directory.
  * debian/compat: bump to 8.

 -- Ludovic Brenta <lbrenta@debian.org>  Tue,  6 Sep 2011 11:30:15 +0200

libxmlada (3.2-5) unstable; urgency=low

  * debian/control (*-dev): depend on gnat, ada-compiler.
    (*-doc, *-dbg): suggest gnat, gnat-4.4, ada-compiler.
    (Standards-Version): bump to 3.8.4 with no changes required.
    (VCS-Browser, VCS-Mtn): new.
    (Architecture): add armel  Closes: #568447.
  * debian/rules: extract the aliversion and soversion from
    debian/changelog.
  * Add support for building architecture-dependent and
    architecture-independent packages separately (i.e. dpkg-buildpackage
    -A, -B).
    debian/rules (binary): split into binary-arch and binary-indep.
    debian/control (Build-Depend-Indep): new, move some dependencies
    from Build-Depend.

  [Nicolas Boulenguez]
  * debian/rules (libxmlada$(ALIVERSION)-dev): Removed obsolete and empty
    doc directory.

 -- Ludovic Brenta <lbrenta@debian.org>  Wed, 16 Jun 2010 20:51:18 +0200

libxmlada (3.2-4) unstable; urgency=medium

  * debian/build_xmlada.gpr: work around a ia64-only compiler bug.
    Allows the package to build on ia64.
  * debian/rules (binary-indep): do not compress PDF files, they are
    already compressed internally.

 -- Ludovic Brenta <lbrenta@debian.org>  Sun, 13 Dec 2009 15:37:21 +0100

libxmlada (3.2-3) unstable; urgency=low

  [Xavier Grave]
  * debian/rules (libxmlada$(ALIVERSION)-dev): s/SONAME/SOVERSION/;
    fixes a bug.

 -- Ludovic Brenta <lbrenta@debian.org>  Tue,  1 Dec 2009 22:04:15 +0100

libxmlada (3.2-2) unstable; urgency=low

  * debian/control (libxmlada-doc): conflict with old versions of
    the -dev package, they used to contain the documentation.
    Closes: #557497.
  * debian/rules (libxmlada-doc): also install the changelog.

 -- Ludovic Brenta <lbrenta@debian.org>  Tue, 24 Nov 2009 00:37:12 +0100

libxmlada (3.2-1) unstable; urgency=low

  * New upstream version taken from Subversion revision 150712.
  * debian/control (Build-Depends, Depends): transition to gnat-4.4.
    (Architectures): remove alpha due to http://gcc.gnu.org/PR42073.
    (libxmlada-doc): new package, Architecture: all.
    Package renamings:
    - libxmlada-dev to libxmlada3.2-dev
      with Conflicts: and Replaces:, per Debian Policy for Ada
      [http://people.debian.org/~lbrenta/debian-ada-policy.html].
    - libxmlada3{,-dbg} to libxmlada3.2{,-dbg}
  * build_xmlada.gpr, xmlada-config.gpr: move to debian/.
  * debian/README.Debian: remove, outdated.

 -- Ludovic Brenta <lbrenta@debian.org>  Thu, 19 Nov 2009 23:06:09 +0100

libxmlada (3.0-6) unstable; urgency=low

  * debian/control (Architecture): add kfreebsd-amd64.  Closes: #542416.
    (Standards-Version): bump to 3.8.3.
    (Depends): s/source:Version/binary:Version/g.
  * debian/rules, debian/control (libxmlada3-dbg): introduce a new
    package containing the detached debugging symbols.
  * build_xmlada.gpr: always build with -g.
  * patches: move to debian/patches.  Make the patches compliant with
    "quilt refresh -p ab".  Closes: #485098.
  * debian/rules: become compliant with source format 3.0 (quilt).
    (clean): s/dh_clean -k/dh_prep/.
    (libxmlada.so.3): gatmake knows about -fPIC when Library_Kind is
    "dynamic", so do not pass it explicitly.
  * debian/compat: bump to 7.
  * debian/xmlada.gpr: turn into a proper library project file.
    Closes: #522031.

 -- Ludovic Brenta <lbrenta@debian.org>  Sat, 12 Sep 2009 12:54:47 +0200

libxmlada (3.0-5) unstable; urgency=low

  * Tighten build-dependency on gnat-4.3 due to bug fixes in libgnat that
    affect the .ali files.
  * Add Support for ppc64 in debian/control.
  * xmlada-config.1: correct a syntax error detected by lintian; simplify.

 -- Ludovic Brenta <lbrenta@debian.org>  Sun,  1 Jun 2008 15:41:26 +0200

libxmlada (3.0-4) unstable; urgency=low

  [Andreas Metzler]
  * Add texlive-latex-base to build-depends. Closes: #479926

 -- Ludovic Brenta <lbrenta@debian.org>  Tue, 13 May 2008 22:09:06 +0200

libxmlada (3.0-3) unstable; urgency=low

  * debian/source.lintian-overrides: new.
  * debian/compat: new.
  * debian/control: use ${source:Version} instead of ${Source-Version}.
    Update Standards-Version to 2.7.3 with no changes.  Add a section
    in the source section.
  * patches/GPL.patch: everything downloaded from libre.adacore.com
    is GPL, no matter what the files themselves say; so, change the
    license of the documentation to the GPL.

  [Xavier Grave]
  * debian/xmlada-config.1: new.
  * debian/xmlada.gpr: revert to a regular project file; library project
    files cause strange problems in very complex scenarios.

 -- Ludovic Brenta <lbrenta@debian.org>  Thu,  1 May 2008 15:30:31 +0200

libxmlada (3.0-2) unstable; urgency=low

  * debian/rules (clean): really remove all generated files.
  * Revert to the upstream version of texinfo.tex.
  * debian/control (libxmlada-dev): fix the dependency on libxmlada3.

 -- Ludovic Brenta <lbrenta@debian.org>  Sun, 20 Apr 2008 23:44:39 +0200

libxmlada (3.0-1) unstable; urgency=low

  [Ludovic Brenta]
  * New upstream release taken from the Subversion repository (rev 120472).
  * Rename the source package to libxmlada since I don't maintain multiple
    versions in parallel.
  * Change the soname to libxmlada.so.3.
  * Migrate to GCC 4.3.  Use a library project file to build the libraries.
  * Add support for mips and mipsel.
  * Rename xmlada2.gpr to xmlada.gpr and turn it into a library project
    file.
  * Migrate to texlive-generic-recommended and texlive-fonts-recommended.
  * Update the patches.

  [Xavier Grave]
  * Provide /usr/bin/xmlada-config.

 -- Ludovic Brenta <lbrenta@debian.org>  Sat, 19 Apr 2008 16:24:46 +0200

libxmlada2 (2.2-7) unstable; urgency=low

  * debian/control (Architecture): add alpha and s390.

 -- Ludovic Brenta <lbrenta@debian.org>  Wed, 11 Oct 2006 21:38:38 +0200

libxmlada2 (2.2-6) unstable; urgency=low

  * Reupload with the pristing source package. D'oh.  I need vacation.

 -- Ludovic Brenta <lbrenta@debian.org>  Wed, 26 Jul 2006 19:18:34 +0200

libxmlada2 (2.2-5) unstable; urgency=low

  * debian/control (libxmlada2): remove mips, mipsel and s390, because
    libgnat is disabled on these architechures.  Closes: #378372.

 -- Ludovic Brenta <lbrenta@debian.org>  Tue, 25 Jul 2006 19:20:59 +0200

libxmlada2 (2.2-4) unstable; urgency=low

  * debian/control: remove mips mipsel s390, because libgnat is disabled on
    these architechures.  Closes: #378372.

 -- Ludovic Brenta <lbrenta@debian.org>  Fri, 21 Jul 2006 21:06:54 +0200

libxmlada2 (2.2-3) unstable; urgency=low

  * debian/control: build-depend on tetex-extra.  Closes: #378432.

 -- Ludovic Brenta <lbrenta@debian.org>  Tue, 18 Jul 2006 07:18:01 +0200

libxmlada2 (2.2-2) unstable; urgency=low

  * debian/control: build-depend on tetex-bin.  Closes: #378432.  Thanks,
    Julien Danjou.  Fix the dependency of libxmlada2-dev.

 -- Ludovic Brenta <lbrenta@debian.org>  Sun, 16 Jul 2006 15:33:32 +0200

libxmlada2 (2.2-1) unstable; urgency=low

  * debian/control:
    - add support for amd64 hppa ia64 mips mipsel s390
    - change libxmlada1{-dev,} to libxmlada2{-dev,}.
    - change my email address: now a full Debian Developer!
    - build-depend on gnat (>= 4.1) and quilt.
    - update Standards-Version to 3.7.2 with no changes.
  * debian/rules:
    - change libxmlada.a to libxmlada2.a,
             xmlada.gpr  to xmlada2.gpr,
             xmlada.info to xmlada2.info
    - change the soname of libxmlada.so.1 to libxmlada.so.2.
    - bypass upstream's configure and make passes.  Just use one
      makefile (debian/rules itself) and one project file.
    - do not install xmlada-config anymore.
  * debian/build_xmlada.gpr: new.
  * debian/xmlada-config.1: remove.
  * debian/copyright: switch to pure GPL, as per upstream.  Explain.
  * libxmlada2-dev can coexist with libxmlada1-dev.
  * patches/series: new, use quilt.
  * patches/info-dir-section.patch, patches/GPL.patch: new.

 -- Ludovic Brenta <lbrenta@debian.org>  Fri, 14 Jul 2006 18:06:30 +0200

libxmlada1 (1.0-3) unstable; urgency=low

  * debian/control: Add support for GNU/kFreeBSD, per request from Aurélien
    Jarno.  Build-depend on gnat (>= 3.15p-19).  Closes: #345065.
  * debian/control: Bump standards-version to 3.6.2 with no changes.
  * debian/control: Update maintainer's email address.
  * debian/libxmlada1-dev.postinst: remove, not necessary anymore.

 -- Ludovic Brenta <ludovic@ludovic-brenta.org>  Wed,  8 Feb 2006 18:22:48 +0100

libxmlada1 (1.0-2) unstable; urgency=low

  * Added a postinst script to make the .ali files read-only.
    Closes: #230260.

 -- Ludovic Brenta <ludovic.brenta@insalien.org>  Thu, 29 Jan 2004 16:29:01 +0100

libxmlada1 (1.0-1) unstable; urgency=low

  * Initial Release.  Closes: #224763.

 -- Ludovic Brenta <ludovic.brenta@insalien.org>  Sat, 20 Dec 2003 20:07:23 +0100
